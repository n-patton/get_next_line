/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: npatton <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/19 15:02:58 by npatton           #+#    #+#             */
/*   Updated: 2018/05/07 15:45:29 by npatton          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *s)
{
	int		i;
	int		l;
	long	n;

	i = 0;
	l = 0;
	n = 0;
	while (ft_isspace(s, i) == 1)
		i++;
	if (s[i] == '-' || s[i] == '+')
		l = i++;
	while (s[i])
	{
		if (n >= 922337203685477580 && (((s[i] - '0') > 8 && s[l] == '-') ||
										((s[i] - '0') > 7 && s[l] != '-')))
			return (s[l] == '-' ? 0 : -1);
		if (s[i] >= '0' && s[i] <= '9')
			n = n * 10 + (s[i] - '0');
		else if (!(s[i] >= '0' && s[i] <= '9'))
			break ;
		i++;
	}
	return (s[l] == '-' ? (n * -1) : n);
}
